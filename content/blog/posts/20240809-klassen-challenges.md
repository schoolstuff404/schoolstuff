---
title: "Klassen-Challenges"
date: 09.08.2024
draft: true
Author: "Aaron und Christian"
featured_image: '/images/default.jpeg'
---

<!-- {{< rawhtml >}}
<video width="25%" controls>
  <source src="shark-attack.mp4" type="video/mp4">
Shark Attack Demo Video
</video>
{{</ rawhtml >}} -->

## Die Idee

Um den Klassenzusammenhalt und die Gemeinschaft zu stärken, hatte Christian die Idee, jede Woche eine Klassen-Challenge aufzugeben.
Die Challenges sind immer etwas lustiges, ausgefallenes, das die SuS als Klasse erreichen müssen.
Idealerweise wird dabei Zusammenarbeit gefordert, was den Klassengeist stärken sollte.

Nach erfolgreichen Abschluss einer Wochen-Challenge erhält die Klasse eine Belohnung in einer Form, die zur Klasse passt.

## Mögliche Challenges

- Die ganze Klasse stellt sich in der Pause in Form einer Blume auf und lässt sich von der Pausenaufsicht fotografieren.
- Die Klasse erstellt eine Liste mit den Lieblingsspeisen aller Lehrpersonen des Schulhauses.
- Die Klasse ...
