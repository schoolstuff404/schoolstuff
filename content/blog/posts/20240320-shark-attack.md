---
title: "Making: Shark Attack"
date: 20.03.2024
draft: false
Author: "Aaron, Kristina, Noelia und Stefan"
featured_image: '/images/shark.jpeg'
---

{{< rawhtml >}}
<video width="25%" controls>
  <source src="shark-attack.mp4" type="video/mp4">
Shark Attack Demo Video
</video>
{{</ rawhtml >}}

In der Blockwoche des Schwerpunktstudiums "Making macht Schule" mussten wir in der Gruppe ein Making-Projekt realisieren.

### Vorgaben

Unser Projekt musste sich mit einem der [17 Nachhaltigkeitsziele der UN](https://www.eda.admin.ch/agenda2030/de/home/agenda-2030/die-17-ziele-fuer-eine-nachhaltige-entwicklung.html) befassen und ein Problem,
das mit einem dieser Ziele in Verbindung gebracht werden kann, lösen.
Wir entschieden uns für das neunte Ziel "Industrie, Innovation und Infrastruktur".

Zweitens sollten wir uns herausfordern und Neues ausprobieren.
Wir hatten noch nie Metall verarbeitet und waren auch im 3D-Druck eher unerfahren, 
weshalb dies in unserem Projekt von zentraler Bedeutung sein sollte.

### Inspiration

In der Mensa und allgemein an der PHSG wird sehr viel ElTony Mate aus Aluminiumdosen konsumiert.
Diese Dosen brauchen in der Entsorgung sehr viel Platz.
Deshalb entschieden wir uns, eine Dosenpresse zu bauen, die in der Mensa im Mariaberg motiert werden soll.

Zudem liessen wir uns von der Kampagne ["Plastikflut stoppen -- Wir haben die Schnauze voll"](https://www.wwf.de/fileadmin/fm-wwf/Publikationen-PDF/Poster-Plastikflut-stoppen-Wir-haben-die-Schnauze-voll.pdf) 
des WWF inspirieren und entschieden uns, die Dosenpresse in die Form eines Hai-Kopfes zu bringen.

### Vorgehen

Die Presse wurde aus Stahl gefertigt und hierbei wurde hauptsächlich geschweisst und genietet.
Der Hai-Kopf und der Dosenzähler wurde mit dem 3D-Drucker produziert.

### Fazit

Wir sind stolz auf unser fertiges Produkt.
Die meisten anfänglichen Schwierigkeiten konnten behoben werden.
Unsere grösstes Ziele, eine funktionstüchtige Dosenpresse zu konstruieren haben wir erreicht.
Der Zähler konnte gedruckt und angebracht werden.
Das Auslösen des Zählers funktioniert jedoch noch nicht.
Dies könnte man noch verbessern.

