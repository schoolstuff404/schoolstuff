---
title: "Jassen in der Schule"
date: 04.04.2024
draft: false
Author: "Aaron"
featured_image: '/images/jass.jpg'
---

Der Jass ist ein Schweizer Gesellschaftsspiel mit einer reichen Tradition, das seit Generationen Menschen aller Altersgruppen begeistert.
Seine Vielfalt an Varianten und die tiefe Verwurzelung in der Schweizer Kultur machen ihn zu einem faszinierenden Element des sozialen Lebens.
Insbesondere in Schulen bietet das Jassen nicht nur eine unterhaltsame Freizeitaktivität, sondern fördert auch wichtige Fähigkeiten wie strategisches Denken und soziale Interaktion.

## Spielweisen

Bevor wir uns ins Jassen vertiefen, ist es wichtig, die verschiedenen Spielweisen zu erlernen, um ein stabiles Fundament zu legen.
Wir beginnen mit den grundlegenden Varianten "Obenabe" und "Uneufe", um ein solides Verständnis für das Jassen zu entwickeln.
Die Einführung des Trumpfs empfehle ich zu einem späteren Zeitpunkt, wenn die Grundlagen des Jassens bereits verinnerlicht sind.

Alle genannten Spielweisen werden in der Regel mit vier Spielern gespielt, entweder in Teams zu zweit oder im "Jeder-gegen-Jeden"-Modus.

### Obenabe

Obenabe könnte man als die traditionellste Spielweise des Jassens betrachten.
Hier gewinnt stets die höchste Karte der ausgespielten Farbe den Stich.

Beispiel:

1. Schilten Zehn
2. Schilten Ober
3. Rosen Ass
4. Schilten Sechs

Die zweite Person, die den Schilten Ober spielt, gewinnt diesen Stich, da der Schilten Ober die höchste Karte der ausgespielten Farbe (Schilten) ist.

### Uneufe

Uneufe ist eine Variante, die die Umkehrung von Obenabe darstellt und nach denselben Regeln gespielt wird.
Der Unterschied besteht darin, dass hier die niedrigste Karte der ausgespielten Farbe den Stich macht.

Beispiel:

1. Eicheln Sieben
2. Eicheln Zehn
3. Schellen Acht
4. Eicheln Neun

Der Stich geht an die Person, die die Eicheln Sieben gespielt hat, da sie die niedrigste Karte der ausgespielten Farbe ist.

### Trumpf

Im Trumpf ändert sich im Vergleich zu Obenabe einiges.
Grundsätzlich wird nach Obenabe-Regeln gespielt, jedoch wird eine Farbe als "Trumpf" bestimmt.
Diese Farbe hat dann einige besondere Eigenschaften, die sie über die anderen Farben erheben.

1. Die Rangordnung der Karten ändert sich **innerhalb der Trumpf-Farbe**.
Die Trumpf-Neun (Nell) ist nun die zweithöchste Karte und der Trumpf-Bauer die höchste.
2. Eine Trumpfkarte steht in der Rangfolge immer über den anderen Farben und kann einen Stich gewinnen, auch wenn eine andere Farbe ausgespielt wurde.
3. Der Trumpf-Bauer muss nicht gezeigt werden (er kann in der Hand behalten werden, auch wenn die Trumpf-Farbe gespielt werden muss).

## Einfache Jass-Arten

### [Schellenjass (Herzjass)](https://jassverzeichnis.ch/anleitung-schellenjass-herzjass/)

#### Spielmaterial:
- Ein Jasskarten-Set (Schweizer Blatt mit 36 Karten)

#### Ziel des Spiels:
Das Ziel des Schellenjass ist es, möglichst wenige Karten der Farbe "Schellen" zu sammeln.
Die Spieler:in mit den wenigsten Punkten gewinnt das Spiel.

#### Spielweise:
1.  **Kartenverteilung**: Jede Spieler:in erhält 9 Karten, je eine im Uhrzeigersinn.
2.  **Spielverlauf**: Die Spieler:in rechts des Gebers beginnt mit dem Spiel.
    Alle Spieler:innen spielen je eine Karte und versuchen dabei, möglichst zu verhindern, einen Stich mit Schellen zu erzielen.
    Die Farbe der ersten ausgespielten Karte bestimmt die Farbe der Runde.
    Die Spieler:in mit der höchsten Karte derselben Farbe gewinnt den Stich.
4.  **Punktezählung**: Am Ende jeder Runde werden die Punkte für gemachte Stiche gezählt.
    Für jede Karte der Farbe Schellen wird ein Punkt gutgeschrieben.
    Schaffte es eine Spieler:in, alle neun Schellen zu sichern, werden allen anderen je 9 Punkte geschrieben.
5.  **Spielgewinn**: Das Spiel endet, wenn eine Spieler:in die vorher festgelegte Punktzahl erreicht oder überschreitet.
    Die Spieler:in mit den wenigsten Punkten gewinnt das Spiel.

*Falls eine Spieler:in nicht farbt (also nicht die ausgespielte Farbe spielt) obwohl er könnte, endet das Spiel sofort und der Spieler:in werden die 9 Punkte gutgeschrieben.*

### [Schieber](https://jassverzeichnis.ch/schieber/)

Beim Schieber spielen die gegenüber sitzenden Spieler:innen in einem Zweierteam.
Das Team mit den meisten Punkten gewinnt das Spiel.

#### Spielmaterial:
- Ein Jasskarten-Set (Schweizer Blatt mit 36 Karten)

#### Ziel des Spiels:
Das Ziel des Schiebers ist es, möglichst viele Stiche zu machen und dadurch Punkte zu sammeln.
Die Punkte werden am Ende jeder Runde gezählt, und das Team mit den meisten Punkten gewinnt das Spiel.

#### Spielweise:
1.  **Kartenverteilung**: Jede Spieler:in erhält 9 Karten, je eine im Uhrzeigersinn.
2.  **Spielansage**: Die Ersthand (erste Spieler:in rechts des Kartengebers) bestimmt eine Spielweise, die für die gesamte Runde gilt.
    Die Spielweisen können Trumpf, Obenabe und Uneufe sein.
3.  **Spielverlauf**: Nach der Spielansage versuchen die Spieler:innen, Stiche zu machen, indem sie eine Karte ausspielen.
    Die Farbe der ersten ausgespielten Karte bestimmt die Farbe der Runde.
    Das Team mit dem höchsten Stichwert gewinnt den Stich.
4.  **Schub**: Wenn die Ersthand ein schlechtes Blatt (schlechte Handkarten) hat, kann sie der gegenüber sitzenden Partner:in schieben.
    Die Partner:in übernimmt dann die Auswahl der Spielweise für die Runde.
5.  **Spielgewinn**: Das Spiel endet, wenn ein Team die vorher festgelegte Punktzahl erreicht oder überschreitet.
    Das Team mit den meisten Punkten gewinnt das Spiel.

Es ist wichtig zu beachten, dass die Ersthand immer die erste Karte spielt, unabhängig davon, ob sie das Spiel angesagt hat oder nicht.

## Weiterführende Links

- [Jassverzeichnis](https://jassverzeichnis.ch)
- [DingsBox Spielekartei](https://dingsbox.schoolstuff.ch)
