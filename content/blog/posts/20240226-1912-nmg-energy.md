---
title: "Energie, Elektrizität und Magnetismus"
date: 26.02.2023
draft: true
Author: "Aaron & Christian"
featured_image: '/images/default.jpg'
---

Wir haben zu den NMG-Themen Energie, Elektrizität und Magnetismus die folgenden Arbeitsblätter erstellt,
die in je einem separaten Skript (mit Lösungen) zum Download verfügbar sind.

Die Arbeitsblätter haben wir in unserem Diplompraktikum in zwei verschiedenen 5. / 6. Klassen ein erstes Mal eingesetzt.

## Energie

Das Skript zum Thema Energie kann
[hier](https://gitlab.com/schoolstuff-ch/sheets/-/raw/main/nmg/energie-magnetismus-elektrizitaet/energie/skript-energie.pdf?ref_type=heads&inline=false)
heruntergeladen werden.

Die Lösungen dazu können
[hier](https://gitlab.com/schoolstuff-ch/sheets/-/raw/main/nmg/energie-magnetismus-elektrizitaet/energie/loesung-energie.pdf?ref_type=heads&inline=false)
heruntergeladen werden.

## Elektrizität

Das Skript zum Thema Elektrizität kann
[hier](https://gitlab.com/schoolstuff-ch/sheets/-/raw/main/nmg/energie-magnetismus-elektrizitaet/elektrizitaet/skript-elektrizitaet.pdf?ref_type=heads&inline=false)
heruntergeladen werden.

Die Lösungen dazu können
[hier](https://gitlab.com/schoolstuff-ch/sheets/-/raw/main/nmg/energie-magnetismus-elektrizitaet/elektrizitaet/loesung-elektrizitaet.pdf?ref_type=heads&inline=false)
heruntergeladen werden.

## Magnetismus

Das Skript zum Thema Magnetismus kann
[hier](https://gitlab.com/schoolstuff-ch/sheets/-/raw/main/nmg/energie-magnetismus-elektrizitaet/magnetismus/skript-magnetismus.pdf?ref_type=heads&inline=false)
heruntergeladen werden.

Die Lösungen dazu können
[hier](https://gitlab.com/schoolstuff-ch/sheets/-/raw/main/nmg/energie-magnetismus-elektrizitaet/magnetismus/loesung-magnetismus.pdf?ref_type=heads&inline=false)
heruntergeladen werden.

