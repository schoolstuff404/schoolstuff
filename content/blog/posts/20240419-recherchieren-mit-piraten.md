---
title: "Lernpfad: Recherchekompetenz"
date: 2024-04-19T11:00:24+02:00
draft: false
featured_image: '/images/default.jpg'
author: ["Aaron", "Magdalena"]
---

# Recherchekompetenzen

In einer Welt, die von einer Fülle von Informationen geprägt ist, ist es von entscheidender Bedeutung, dass Schülerinnen und Schüler die Fähigkeiten entwickeln, diese Informationen kritisch zu hinterfragen, zu bewerten und zu nutzen.
Aus diesem Grund wurde dieser Lernpfad entwickelt, um unseren Schülern die Möglichkeit zu geben, genau das zu tun.

Warum ist es wichtig, dass Recherchekompetenzen entwickelt werden? Recherchekompetenzen sind nicht nur entscheidend für den akademischen Erfolg, sondern auch für das tägliche Leben.
Indem effektive Recherchetechniken erlernt werden, können verlässliche Quellen identifiziert, falsche Informationen erkannt und fundierte Entscheidungen getroffen werden.
Diese Fähigkeiten sind nicht nur in der Schule von Bedeutung, sondern auch in einer zunehmend digitalisierten Welt, in der Fähigkeiten zur Informationsbewertung unerlässlich sind.

Unser Lernpfad bietet Schülerinnen und Schülern die Möglichkeit, auf eine interaktive und praxisorientierte Weise in die Welt der Recherchekompetenzen einzutauchen.
Sie werden durch verschiedene Lernaktivitäten geführt, die ihnen helfen, die notwendigen Fähigkeiten zu entwickeln, um sich in einer komplexen Informationslandschaft zurechtzufinden.

Wir laden Sie herzlich ein, diesen Lernpfad zu erkunden und Ihre Schülerinnen und Schüler auf eine Reise zu führen, die sie mit den Fähigkeiten ausstattet, die sie für ein lebenslanges Lernen und Erforschen benötigen.

## Der Lernpfad

{{< rawhtml >}}
<div style="width: 100%;">
    <div style="position: relative; padding-bottom: 56.25%; padding-top: 0; height: 0;">
        <iframe title="Recherchepiraten" frameborder="0" width="1200" height="675" style="position: absolute; 
            top: 0; left: 0; width: 100%; height: 100%;" 
            src="https://view.genial.ly/661d0b67b279ce0015715be8" 
            type="text/html" allowscriptaccess="always" allowfullscreen="true" scrolling="yes" allownetworking="all">
        </iframe>
    </div>
</div>
{{</ rawhtml >}}


### Entstehungsgeschichte

{{< rawhtml >}}
<iframe width="760px" height="500px" src="https://sway.cloud.microsoft/s/CrziHFyu84BHSaRN/embed" frameborder="0" marginheight="0" marginwidth="0" max-width="100%" sandbox="allow-forms allow-modals allow-orientation-lock allow-popups allow-same-origin allow-scripts" scrolling="no" style="border: none; max-width: 100%; max-height: 100vh" allowfullscreen mozallowfullscreen msallowfullscreen webkitallowfullscreen></iframe>
{{</ rawhtml >}}

## Portfolio

[Download (docx)](./piraten-portfolio.docx)

