---
title: "datenschutz"
featured_image: '/images/default.jpg'
---

# Datenschutzerklärung für schoolstuff.ch

Wir bei schoolstuff.ch nehmen den Schutz Ihrer persönlichen Daten sehr ernst. Diese Datenschutzerklärung informiert Sie darüber, wie wir mit Ihren Daten umgehen, wenn Sie unsere Website besuchen.

## 1. Keine Speicherung von Nutzerdaten

schoolstuff.ch speichert keinerlei personenbezogene Daten unserer Besucher. Wir verfolgen oder speichern keine Informationen über Sie, wenn Sie unsere Website besuchen, es sei denn, Sie geben uns freiwillig solche Informationen (z. B. über ein Kontaktformular).

## 2. Cookies

Wir verwenden keine eigenen Cookies auf schoolstuff.ch. Bitte beachten Sie jedoch, dass Drittanbieter, deren Inhalte auf unserer Website eingebettet sind (z. B. eingebettete Videos von YouTube), möglicherweise eigene Cookies verwenden, um ihre Dienste anzubieten und zu verbessern. Diese Cookies unterliegen den Datenschutzrichtlinien der jeweiligen Drittanbieter.

## 3. Externe Links

Unsere Website schoolstuff.ch kann Links zu anderen Websites enthalten, die nicht von uns betrieben werden. Wir übernehmen keine Verantwortung für die Datenschutzpraktiken oder den Inhalt solcher Websites. Wir empfehlen Ihnen, die Datenschutzerklärung jeder Website zu lesen, die Sie besuchen.

## 4. Änderungen dieser Datenschutzerklärung

Diese Datenschutzerklärung kann von Zeit zu Zeit aktualisiert werden, um Änderungen in unseren Datenschutzpraktiken widerzuspiegeln. Bitte überprüfen Sie regelmäßig diese Seite, um sicherzustellen, dass Sie mit unseren aktuellen Datenschutzpraktiken vertraut sind.

## 5. Kontakt

Wenn Sie Fragen oder Bedenken hinsichtlich unserer Datenschutzpraktiken haben, können Sie uns unter [Kontakt-E-Mail](mailto:contact@azureorange.xyz) kontaktieren.

Vielen Dank für Ihr Vertrauen in schoolstuff.ch.


