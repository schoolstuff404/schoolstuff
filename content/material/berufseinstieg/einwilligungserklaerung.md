---
title: "Einwilligungserklärung zur Veröffentlichung von Fotos und Aufnahmen"
featured_image: '/images/default.jpg'
---

Dies ist eine mögliche Einwilligungserklärung. Sie muss noch mit dem Name der Schule angepasst werden. Oft gibt es eine solche Einwilligungserklärung von der Schulgemeinde aus. In diesem Fall sollte diese genutzt werden.

[Beispiel einer Einwilligungserklärung (.docx)](./einwilligungserklaerung-zur-veroeffentlichung-von-fotos-und-aufnahmen.docx)
