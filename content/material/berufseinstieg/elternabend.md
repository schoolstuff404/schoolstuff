---
title: "Elternabend"
featured_image: '/images/default.jpg'
---

## Was ist ein Elternabend

Bei einem Elternabend werden Themen ausgetauscht, welche die ganze Klasse betreffen und nicht einzelne Probleme von individuellen Schüler:innen. Es sind alle Erziehungsberechtigten von den Kindern einer Klasse anwesend (im Gegensatz zum Elterngespräch, bei dem nur die Eltern von einem Kind mit der Lehrperson in den Austausch kommen). Dabei werden Informationen und Termine bekanntgegeben, Sorgen und Probleme diskutiert. Der Elternabend bietet zudem eine gute Plattform, um die Beziehung zwischen, Erziehungsberechtigten, Lehrperson und Schule zu pflegen. 

## Vorbereitung

[Beispielpräsentation Elternabend (.pptx)](./elternabend-beispielpraesentation.pptx)

### Einladungsschreiben 

Der Termin sollte mindestens zwei Wochen im Voraus bekanntgegeben werden und vorgängig mit der Schulleitung und dem Hausdienst abgesprochen werden. 
Folgende Punkte sollten auf der Einladung aufgeführt sein:

- Datum
- Uhrzeit
- Ort
- Die geplante Tagesordnung
- Möglichkeit für Erziehungsberechtigte für Themenwünsche

### Tagesordnung

Es empfiehlt sich eine Tagesordnung mit den wichtigsten Themen bekannt zu geben, damit die Anwesenden eine Ahnung von dem Ablauf erhalten. Eine kurze Auflistung auf der Wandtafel oder auf einer PowerPoint-Folie reichen völlig aus. 

### Klassenzimmer vorbereiten

Sind im Klassenzimmer ausreichend Stühle vorhanden? Sind der Raum und die Wandtafel sauber? Auch diese Punkte solltest du bei der Vorbereitung berücksichtigen 

### Sitzordnung

Achte bei der Sitzordnung darauf, dass sich alle sehen können. Einen Stuhlkreis oder eine U-Form bieten sich dazu besonders an. Die Atmosphäre wird dadurch lockerer und die Beteiligten verschanzen sich nicht hinter den Tischen. 

### Namensschilder

Bereite Namensschilder vor mit dem Namen der Eltern und des Kindes. Dadurch kannst du alle beim Namen nennen und weisst direkt welche Personen zu welchem Kind gehören. 

## Übliche Themen für die Tagesordnung

### Klassengemeinschaft

Eines der beliebtesten Themen beim Elternabend ist die soziale Stimmung in einer Klasse. Beschreibe diese möglichst prägnant (möglicherweise vorher eine Befragung in der Klasse machen) und erkundige dich bei den Eltern nach Rückmeldungen seitens der Kinder zuhause. Schildere den Eltern bei Bedarf die Probleme und die Lösungsansätze dazu. 

### Lernstoffvermittlung

Welche Themen sollen in diesem Jahr behandelt werden, was sieht der Lehrplan vor und mit welchen Methoden arbeitest du? Solche Fragen interessieren die Erziehungsberechtigten. Fasse dich trotzdem kurz und verliere dich nicht ins Detail. 

### Tipps für Eltern

Gib den Eltern Tipps, wie sie ihren Kindern bei den Hausaufgaben und beim Lernen helfen können. Auch allgemein interessiert es die Eltern, wie sie den Kindern beim Bewältigen des Schulalltages unter die Arme greifen können. 

### Notengebung

Obwohl unter Lehrpersonen der Einsatz von Noten kontrovers diskutiert wird, sind diese für die Eltern immer noch der beste Leistungsindikator. Natürlich bist du nicht verpflichtet Noten zu geben. Informiere am Elternabend jedoch in jedem Falle, wie du die Leistungsnachweise bewertest und wie sich eine Zeugnisnote zusammensetzt. 

### Klassenregeln

Informiere die Eltern über die Klassenregeln. Möglicherweise musst du gegen ein Kind eine Konsequenz aussprechen und das Wissen um die Regeln fördert möglicherweise das Verständnis seitens der Erziehungsberechtigten über deine Entscheidung. 

### Themenschwerpunkte

Je nach Stufen bieten sich unterschiedliche Themen an: Hausaufgaben, Medienkompetenz, Sexualkunde, Mobbing, Berufsorientierung, etc. Auch Praktika und Stellvertretungen sollten am Elternabend angesprochen werden. 

## Darauf solltest du achten

- Behalte die Zeit im Auge – Für die Dauer des Elternabends solltest du nicht mehr als zwei Stunden einplanen
- Bei den meisten Punkten hast du die Zügel in der Hand und lenkst das Geschehen. Vergiss aber nicht die Eltern immer wieder miteinzubeziehen und biete eine Plattform für Fragen und Erfahrungen. 
- Nimm die Anliegen der Eltern ernst, auch wenn du nicht immer gleicher Meinung bist. Die Kommunikation mit den Erziehungsberechtigten ist für Lehrpersonen essenziell. Durch Aufmerksamkeit und Gesprächsbereitschaft beflügelst du eine erfolgreiche Kommunikationsebene.
- Gib deine Handynummer nicht leichtfertig heraus. Die Eltern haben die Möglichkeit dich über das Sekretariat oder per Mail zu erreichen.
unten findest du eine Beispiel PowerPoint-Präsentation.

