---
title: "Absenzenwesen"
featured_image: '/images/default.jpg'
---

![logo-pupil](./pupil.png)

Hier gab es bei den verschiedenen Interviewpartnern unterschiedliche Meinungen und Systeme zu Kommunikation mit den Eltern. Wir stellen euch die Möglichkeit mit der App Pupil vor, denn diese wird ab dem Schuljahr 24/25 in St. Galler Schulen obligatorisch eingeführt und soll auch gängige Plattformen wie das Lehrer Office ersetzen.

Die Grund Idee der App ist sehr einfach und die Kommunikation mit den Eltern funktioniert ähnlich wie bei gängigen Messengern wie Whatsapp. Die Plattform Pupil bietet sowohl die Möglichkeit mit den Elternteilen im Gruppenchat, sowie in Einzelchats zu kommunizieren. Die Eltern der Kinder müssen dabei lediglich die App auf ihrem Gerät installieren und werden dann von der Klassenlehrperson in eine Gruppe eingeladen. Somit findet die gesamte Kommunikation in Zukunft nur noch über diese App statt. Ist beispielsweise ein Kind krank, so kann das Elternteil dies der Lehrperson im Pupil Chat mitteilen. Die App bringt den grossen Vorteil, dass die gesamte Kommunikation nur noch über einen Kanal stattfinden und sie sehr bedienungsfreundlich ist. Ausserdem wird bei der App Wert auf Datenschutz gelegt. Die gesamten Nutzerdaten der Lehrperson, sowie die der Elternteile werden auf einem Firmeninternen Server in der Schweiz gespeichert und nicht weitergegeben. Ausserdem macht die App damit mühsame Anrufe der Eltern am Morgen, um ihr Kind abzumelden, vergessen. Ein Interviewpartner, welcher die App seit diesem Jahr bereits einsetzt, hat uns durchwegs positives darüber berichtet. Allerdings ist es wichtig am Anfang den Eltern klar und deutlich zu kommunizieren, dass die Kommunikation ausschliesslich über die App stattfindet und keine Telefonanrufe (im Normalfall) beantwortet werden. Dies soll wirklich nur im Notfall geschehen.
