---
title: "Checkliste für berufseinsteigende Lehrpersonen"
featured_image: '/images/default.jpg'
---

Hier siehst du eine Checkliste, welche die PH Luzern für Berufseinsteiger zusammengestellt hat. Du erkennst auf einen Blick, um welche Anliegen du dich kümmern musst und welche dein/e Mentor/in für dich übernimmt, bzw. dich dabei unterstützt.

[Checkliste Berufseinstieg (.pdf)](./checkliste-berufseinstieg.pdf)
