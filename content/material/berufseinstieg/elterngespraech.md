---
title: "Elterngespräch"
featured_image: '/images/default.jpg'
---

## Was ist ein Elterngespräch?

Ein Elterngespräch ist ein Austausch zwischen Lehrperson und Eltern. Sie diskutieren über Leistungen, soziales Verhalten oder Probleme eines Kindes. Im Gegensatz zum Elternabend erscheinen beim Elterngespräch nur die Eltern eines Kindes.

## Vorbereitung

### Materialien bereithalten

Halte Klassenarbeiten, Tests, Hefte und Hausaufgaben, Beobachtungsbogen, Leistungsstand bereit, um allfällige Probleme zu verdeutlichen.

### Sicht der Schüler:innen befragen

Erkundige dich bei dem entsprechenden Kind nach seiner/ihrer Sichtweise bezüglich des aktuellen Stands hinsichtlich Leistungen, Probleme, Verbesserungsvorschlägen, etc.

### Klassenzimmer vorbereiten

Achte darauf, dass das Klassenzimmer ordentlich aussieht und kein Abfall herumliegt. 

### Störungen vermeiden

Du kannst sogar ein Schild mit “Bitte nicht stören“ an der Türe anbringen, um den Eltern zu zeigen, dass du dem Gespräch Priorität einräumst. 

## Während des Elterngespräch

### Positive Stimmung

Eine nette Begrüssung und ein wenig Smalltalk schaffen eine gute Stimmung. Behalte während des Gesprächs stets einen höflichen Ton bei und biete Getränke und evtl. kleine Naschereien an.

### Ablauf erläutern

Zeige kurz die zu besprechenden Punkte auf.

### Zeitmanagement

Behalte die Zeit gut im Auge, um bei nachfolgenden Elterngesprächen nicht in Zeitnot zu geraten.

### Notizen

Halte die wichtigsten Gesprächsinhalte fest und nimm Vereinbarungen und Hinweise der Eltern auf.

### Nicht nur auf Negatives Hinweisen

Weise auch bei Kindern mit mehr Schwierigkeiten auf die Stärken und positiven Eigenschaften hin.

### Elternsicht

Gib den Eltern die Gelegenheit Fragen zu stellen, Wünsche zu äussern und ihre Sichtweise darzulegen.

### Zusammenfassung

Fasse nochmals die wichtigsten Punkte und Vereinbarungen zusammen.

## Schlussphase

### Tipps geben

Gib den Eltern Tipps, wie sie ihr Kind am besten unterstützen können und welche Fördermassnahmen den Zweck am besten erfüllen. 

### Verabschiedung

Verabschiede dich höflich und erkundige dich allenfalls nach einem gewünschten Folgetermin. 

### Nach dem Gespräch

Fertige ein kurzes Gesprächsprotokoll an und leite es den Eltern bei Bedarf weiter.
