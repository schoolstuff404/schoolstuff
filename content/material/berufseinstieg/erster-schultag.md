---
title: "1. Schultag"
featured_image: '/images/default.jpg'
---

Diesen Vorschlag haben wir anhand mehrerer Lehrerinterviews zusammengestellt und er entspricht einem 1. Schultag nach unseren Vorstellungen. Es gibt natürlich sehr viele Möglichkeiten einen tollen Einstieg mit der Klasse zu gestalten und wir wollen dir nur eine Option präsentieren.
Viele Schulen haben ein klassenübergreifendes Ritual für den Einstieg. Da diese je nach Einrichtung stark variieren bezieht sich unser Vorschlag auf deinen ersten Moment mit der Klasse im Schulzimmer.

## Klassenzimmer

Der erste Eindruck des Klassenzimmers ist besonders wichtig. Versuche eine einladende Atmosphäre zu schaffen mit schönen Wandtafelbildern, einem sauberen Raum und je nach Altersstufe mit einem Klassenmaskottchen.

## Begrüssung

Begrüsse alle Kinder und evtl. die Eltern höflich und gib ihnen gleich ein wohliges Gefühl. Achte dabei auf Augenkontakt.

## Namensschilder

Es ist deiner Entscheidung überlassen, ob du die Namenschilder selbst vorbereitest oder die Kinder diese selbst gestalten lässt. Für Erstklässler empfehlen wir die Arbeit selbst zu übernehmen und vielleicht auf dem Namensschild ein Tier mit dem Anfangsbuchstaben anzufügen.
Kinder aus höheren Jahrgängen schätzen jedoch mehrheitlich, wenn sie die Gestaltung des Namensschildes selbst übernehmen dürfen.

## Ablauf

Zeigen den Kindern kurz auf, was sie an diesem Tag alles erwartet. Somit schaffst du Sicherheit und die Schüler:innen können sich besser auf den Tag einstellen.

## Klassenzimmerrundgang

Führe die Kinder im Klassenzimmer herum und zeige ihnen die Materialausstattung und die Besonderheiten des Raumes.

## Platz einrichten

Klar haben die Kinder zu Beginn noch kaum Material. Trotzdem vermittelt die Kenntnis über den eigenen Platz Orientierung und Sicherheit. Wir empfehlen beim ersten Schultag die Schüler:innen den Platz noch selbst aussuchen zu lassen, da du das Sozialverhalten der Kinder noch schlecht einschätzen kannst. Mit der Zeit kannst du die Sitzordnung immer noch anpassen. Ein regelmässiger Platztausch kann der soziale Austausch und den Klassenzusammenhalt stärken.

## Spiele

Anschliessend empfehlen wir ein kurzes Spiel zu machen. Falls sich nicht alle in der Klasse kennen, empfiehlt sich ein Kennenlernspiel. Spiele sind ein geeignetes Mittel, um eine lockere Atmosphäre zu schaffen und erfreuen sich grosser Beliebtheit bei den Kindern. Du kannst auch mehrere Spiele durchführen. Unter Kennenlernspiele findest du eine geeignete Ideensammlung.

## Austausch ermöglichen

Egal ob sich die Kinder bereits kennen oder nicht, ermögliche eine Austauschrunde. Gerade bei Schüler:innen, die sich nicht kennen kann Gesprächsmangel herrschen. Halte für diesen Fall einige Fragen und Gesprächsthemen bereit, über die die Kinder diskutieren können.

## Plakate gestalten

Ein weiteres Medium, um die Kinder besser kennenzulernen, sind von den Kindern gestaltete Plakate. Mache ein Foto von den einzelnen Schüler:innen und drucke dieses aus. Die Kinder können das Foto auf ein Plakat kleben und etwas über sich erzählen (in gestalterischer Form). Alternativ können die Kinder sich selbst auch zeichnen, oder ausmalen auf einem vorgängig ausgedruckten Gesicht (nur Umriss). Es kann auch gleich das Geburtsdatum darauf vermerkt werden, um eine zusätzliche Erinnerung zu haben.

## Aktivität als Klasse

Um das Wohlbefinden und den Zusammenhalt der Klasse zu stärken, eignen sich Klassenaktivitäten. Die Kinder haben dabei eine gemeinsame Mission und müssen zusammenarbeiten, um zum Ziel zu kommen. Dies können Spiele sein, bei denen die gesamte Klasse ein gemeinsames Ziel verfolgt (z.B. einen Bogen mit verschiedenen Herausforderungen, welche innerhalb einer Stunde erreicht werden müssen) oder andere Aktivitäten (zusammen im Schulhausgarten Tomaten anpflanzen). Je nach Schulinfrastruktur bieten sich einige Möglichkeiten besser an und andere sind weniger geeignet.

## Gemeinsamer Schluss

Versammle nochmals alle Kinder und gestalte einen gemeinsamen Schluss. Hier bieten sich zum Beispiel musikalische Ausklänge an. Du kannst dabei auch nach Draussen gehen und an einem gemütlichen Ort einen musikalischen Abschied machen. Dieser fährt die Schüler:innen deiner Klasse nach einem ereignisreichen Tag etwas herunter. Hierfür bieten sich Gemeinschaftslieder wie «mir sind e Klass» an. Das Gefühl der Zusammengehörigkeit kann so unbewusst gestärkt werden.

## Info am Rande

Wie bereits erwähnt ist dies ein möglicher Vorschlag. Du kannst auch einfach nur einzelne Elemente davon in deinen ersten Schultag einbauen oder einzelne Sequenzen weglassen. Lasse deiner Kreativität freien Lauf und traue dich etwas, auch wenn dabei etwas vielleicht nicht ganz nach Wunsch gelingt.

Und denke immer daran: Es ist noch kein Meister vom Himmel gefallen!

Viel Spass! 😊






