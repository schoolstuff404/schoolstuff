---
title: "Hausordnung"
featured_image: '/images/default.jpg'
---

Dies ist eine mögliche Hausordnung aus einer Primarschule. Die Schulhausordnung kann von Schulhaus zu Schulhaus variieren und ist oftmals im Schulhausteam mit allen Beteiligten erstellt worden. Aus verschiedenen Interviews und Beispielen einer Hausordnung haben wir folgende Möglichkeit zusammengestellt. Oft ist es so, dass die Hausordnung bei Stellenantritt bereits besteht und den neuen Klassenlehrpersonen zum Aufhängen im Schulzimmer verteilt wird. Diese sollten dann mit den Schülern und Schülerinnen besprochen werden. Dies ist vor allem wichtig, wenn eine 1. Klasse übernommen wird, da die Kinder da oftmals vom Kindergarten aus einem anderen Schulhaus kommen, wo allenfalls andere Regeln galten.

## Beispiel einer Hausordnung

1. Wir sind pünktlich im Schulzimmer 
2. Beim Eingang reinigen wir unsere Schuhe. 
3. Wir halten Ordnung in den Garderoben. 
4. Private elektronische Geräte bleiben zu Hause. 
5. Wir lassen Rollerblades und Kickboards zu Hause. 
6. Wir verbringen die Pause im Freien und bleiben innerhalb des Schulareals. 
7. Abfälle werden im Kübel entsorgt. 
8. Wir spielen bei nassem Wetter nicht auf der Spielwiese. 
9. Wir werfen und schiessen keine Bälle an die Schulhauswand. 
10. Wir benutzen die empfohlenen Schulwege.

## Fussballplan

In einer von uns besuchten Schule kam es in der 10 Uhr-Pause immer wieder zu Streitereien um und auf dem Fussballplatz. Deshalb hat sich die Schule dazu entschieden, zusätzlich zu der Hausordnung noch einen Fussballplatzplan zu erstellen:

- Nur im grossen eingezeichneten Feld ist Fussballspielen erlaubt.
- Dieser Plan gilt nur für die Vormittagspause.
- In der Nachmittagspause ist das Feld offen für alle.
  ("offen" -> Schüler:innen sprechen sich selber ab.)

