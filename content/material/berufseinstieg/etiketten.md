---
title: "Etiketten / Beschriftungen"
featured_image: '/images/default.jpg'
---

Dies ist eine Vorlage für Etiketten, um die Bücher und Hefter der Schüler:innen anzuschreiben. Die verschiedenen Bilder sind einige Beispiele von Kategorien, die genutzt werden könnten. Das Bild sollte jedoch pro Kind immer das gleiche sein für alle Bücher und Hefter. 

Beispiele für Kategorien sind: 

- Monster 

![Monster](./monster.jpeg)

- Tiere

![Tiere](./tiere.png)

- Früchte/Gemüse 

![Obst](obst.jpeg)

- Sportarten 

![Sportarten](./sport.jpeg)

- Farben 

![Farben](./farben.jpeg)

[Beispiel Etiketten (.docx)](./etiketten-vorlage.docx)
