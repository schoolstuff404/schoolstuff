---
title: "Elternbriefe"
featured_image: '/images/default.jpg'
---

Dies ist eine Art einen Elternbrief aufzubauen. Als Unterseiten sind Beispiele von möglichen Elternbriefen. Diese Beispiele haben wir von verschiedenen Lehrpersonen erhalten, die wir befragt haben.

## Beispielhafter Aufbau

Begrüssung: Freundliche Ansprache (Bsp.: Liebe Eltern)

Einleitung: Zweck des Briefes (Bsp.: Ich schreibe diesen Brief, um einige Eckdaten bekannt zu geben)

Informationen: Eckdaten oder andere Informationen bekannt geben (Bsp.: Die Schulreise findet am ____ statt.)

Unterschrift: Verabschiedung mit Unterschrift und Datum (Bsp.: Freundliche Grüsse __________)

Evtl. Anhänge: Formulare oder weitere Dokumente (Bsp.: Einschreibung Fahrgemeinschaft)

## Beispiele von Elternbriefen

[Beispiel 1 (.pdf)](./elternbrief-beispiel-1.pdf)

[Beispiel 2 (.pdf)](./elternbrief-beispiel-2.pdf)

[Beispiel 3 (.pdf)](./elternbrief-beispiel-3.pdf)
