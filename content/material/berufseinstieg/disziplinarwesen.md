---
title: "Disziplinarwesen"
featured_image: '/images/default.jpg'
---

Hier ist die Handhabung von Schulgemeinde zu Schulgemeinde und von Lehrperson zu Lehrperson anders. Gemeinsam hatten alle von uns besuchten Schulen, dass bei dem ersten Verstoss gegen Schulhausregeln eine Verwarnung ausgesprochen wird und beim zweiten Vergehen eine Konsequenz in Kraft tritt. Diese können sehr vielfältig sein. Sinnvoll sind dabei aus unserer Sicht Konsequenzen, welche dem Allgemeinwohl im Schulhaus helfen und nicht sinnfreies Regelabschreiben, wie wir es noch aus unserer Schulzeit kennen. Somit könnten mögliche Konsequenzen sein, dem Schulhaus Abwart eine Stunde zu helfen, die Pausenkiste zu betreuen oder beim Pausenkiosk zu helfen.

Im Gegensatz zu den Schulhausregeln, sind die Regeln im Klassenzimmer von Lehrperson zu Lehrperson unterschiedlich. Im Rahmen unserer Unterrichtsbesuchen und den Interviews mit verschiedenen Klassenlehrpersonen haben wir verschiedene Systeme und Regeln beobachten können. Im Folgenden wird eine Möglichkeit, welche wir als passend empfunden haben, vorgestellt. Diese soll als mögliche Vorlage für ein Belohnungssystem dienen. Jedoch variiert, wie bereits erwähnt, die Umsetzung der einzelnen Lehrperson und zum Berufsstart ist es wichtig herauszufinden, was für den individuellen Bedarf geeignet ist und auf was Wert gelegt werden möchte.

[Beispiel Belohnungskarte (.docx)](./meine-belohnungskarte.docx)

[Beispiel Belohnungskarte (.pdf)](./meine-belohnungskarte.pdf)

Das Ziel ist dabei, mit der Klasse zwei verbindliche Regeln aufzustellen und diese dann auch einzuhalten. Dabei können die Regeln je nach Klasse und Lehrperson individuell sein. Wenn dies fünf mal gut klappt (zum Beispiel fünf mal jeweils einen ganzen Tag ohne Regelübertretung), erreicht man das Feld des Smileys und bekommt eine Belohnung. Dies muss nichts weltbewegendes oder materielles sein. Einige mögliche Belohnungen sind: Einen Tag keine Hausaufgaben, Spiele Wunsch in Bewegungspause oder im Sportunterricht, fünf Minuten früher in die Pause, Ämtli wählen usw.
