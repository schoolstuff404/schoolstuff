---
title: "Ämtliplan"
featured_image: '/images/default.jpg'
---

Dies ist eine Vorlage für einen möglichen Ämtliplan. Dieser Kreis müsste dann ausgedruckt, laminiert und aufgehängt werden. Die Idee ist, dass jedes Kind eine Klüpperli mit dessen Namen hat und diese dann jede Woche an den Plan gemacht werden können. Die Ämtli können nach Wunsch angepasst werden. In diesem Fall habe ich einige Beispiele in die Felder geschrieben. Ebenfalls kann auch die Anzahl der Felder angepasst werden, falls nötig. Dafür müsste jedoch das Diagramm abgeändert werden.

[Ämtliplan Vorlage](./aemtliplan-vorlage.docx)
