---
title: "Aufgabenkontrolle"
featured_image: '/images/default.jpg'
---

Die Umsetzung ist stark Lehrperson abhängig und das folgende Beispiel ist für uns sinnvoll. Deshalb möchten wir es gerne kurz vorstellen.

![hausaufgaben-parkplatz](./hausaufgaben-parkplatz.jpeg)

Die Schülerinnen und Schüler haben alle eine Wäscheklammer mit ihrem Namen am Hausaufgabenparkplatz. Am Morgen, wenn sie in der Schule ankommen, sollen sie als Erstes die Hausaufgaben am Parkplatz abgeben. Dazu heften sie ihre Wäscheklammer an ihre Hausaufgabe und legen es auf den Parkplatz. Die Lehrperson kann so anschliessend genau sehen, wessen Wäscheklammer noch auf der Seite hängt und die Hausaufgaben nicht abgegeben hat.

![kontrollier-box](./kontrollier-box.jpeg)

Gibt es während dem Unterricht weitere Arbeitsblätter zum Kontrollieren, so kommen diese in die bitte kontrollieren Box. Anschliessend kontrolliert die Lehrperson die Aufgaben und gibt diese zurück. Ist alles korrekt, so kommen die Blätter oder Hausaufgaben i die fertig Box der Kinder, wo alles gesammelt wird, bis es eigeordnet wird. Gibt es noch Sachen zum Verbessern, so kommt das Blatt zurück in die Arbeitsbox der Kinder, wo alle angefangenen oder noch zu verbessernden Blätter der Schüler und Schülerinnen sind.

![maennchen](./maennchen.jpeg)
