---
title: "Schüler:innen-Brief zum Schulstart"
featured_image: '/images/default.jpg'
---

Dies ist ein Brief, der den Schüler:innen vor dem Schulstart zukommen sollte. Dies kann durch einen Besuch in der früheren Klasse oder per Post geschehen. Hier wurden wieder um die Monster als Thema genommen.
Diese Bilder sollten an das Thema angepasst werden. Mit diesem Brief wird gleichzeitig auch die Gruppeneinteilung sowie der Stundenplan abgegeben.

[Schüler:innen-Brief Vorlage (.docx)](./schuelerinnen-brief-vorlage.docx)

[Schüler:innen-Brief Vorlage (.pdf)](./schuelerinnen-brief-vorlage.pdf)
