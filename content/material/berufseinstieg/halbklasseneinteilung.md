---
title: "Gruppeneinteilung Halbklassen"
featured_image: '/images/default.jpg'
---

Dies ist eine Vorlage zur Gruppeneinteilung innerhalb der Klasse. Die Bilder der Monster sind als Beispiel gedacht. Hier sollten passende Bilder zu den Gruppennamen eingefügt werden.

[Vorlage Gruppeneinteilung Halbklassen (.docx)](./gruppeneinteilung-halbklasse.docx)
