---
title: "mobbing"
menu: 
    main:
        parent: "material"
featured_image: '/images/default.jpg'
---

In keinem Land sind die Mobbingquoten so hoch wie in der Schweiz!

Deshalb ist es gerade auch bei uns wichtig, Mobbing-Prävention zu betreiben.

## Prävention

Prävention ist das beste und wichtigste, das gemacht werden kann, um Mobbing zu verhindern.

- Bilderbücher:
    - Was ist bloss mit Gisbert los?
    - Tomadenrot
    - Du doof?
- Klassenzusammenhalt stärken
- [SRF Kids](https://www.srf.ch/kids) / [SRF school](https://www.srf.ch/sendungen/school?q=&date=all&page=0&level=all&subject=all)
- Kanton St. Gallen [Mobbing in der Schule : sicher!gsund!](https://sichergsund.ch)
- Mobbing-Prävention sollte fix in die Jahresplanung

## Intervention

Anlaufstellen:

- Lehrpersonen blabla
- Schulsozialarbeit
- Schulleitung
- SDP
- Fachstellen
- Eltern
- No-Blame-Approach [Eine Anleitung für Lehrpersonen](https://www.fritzundfraenzi.ch/schule/no-blame-approach-eine-anleitung-fur-lehrpersonen/)
