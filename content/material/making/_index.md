---
title: "making"
menu: 
    main:
        parent: "material"
featured_image: '/images/default.jpg'
---

## Making in der Primarschule

Im Rahmen von Making-Aktivitäten können die Kinder ihrer Kreativität und Fantasie freien Lauf lassen.
Die einzigen Grenzen, die gesetzt werden, sind die zur Verfügung stehenden Materialien und Werkzeuge.
Die Kinder können dabei sehr vieles über verschiedene Herstellungsverfahren lernen und verschiedenste Technologien miteinander verknüpfen.
Sie lernen dabei auch vieles darüber, wie wichtig es ist, Ideen, Materialien udn Werkzeuge miteinander zu teilen. 
Dies kann in einem Makerspace selbst sein, sich aber auch in ein globales Ausmass ausweiten.
So werden etwa geschriebene Computer-Programme oder 3D-Modelle gern und häufig über das Internet gratis verteilt.

Die Kinder lernen mit Scheitern umzugehen, lernen daraus und machen es das nächste Mal besser.
Das Making schafft einen Grund, um viele verschiedene Techniken zu erlernen, da es Spass macht, diese anzuwenden und miteinander zu verbinden.
So kann das Programmieren beispielsweise plötzlich relevant erscheinen, da es einem Projekt zugute kommt.

Falls jemand an einer Schule einen eigenen Makerspace eingerichtet möchte, finden sich auf der Seite von
[Makerspace Schule](https://makerspace-schule.ch/making-im-schulalltag/)
viele nützliche Hinweise.
Unter anderem bieten sie eine Sammlung an Dokumenten zur [Planung eines Makerspace](https://makerspace-schule.ch/making-im-schulalltag/planen/).

### Makerspaces in der Schweiz

- [Makerspace im RDZ Gossau](https://www.phsg.ch/de/dienstleistung/regionale-didaktische-zentren/rdz-gossau/makerspace)
- [Makerspace PHTG](https://digital-learning-lab.phtg.ch/makerspace/)

Weitere Makerspaces finden sich auf [makerspaces.ch](https://makerspaces.ch).

