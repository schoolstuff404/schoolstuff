---
title: "Laser-Cutting und Plotting"
date: 2023-10-04T20:35:11+02:00
draft: false
Author: "Aaron"
featured_image: '/images/default.jpg'
---

Laser-Cutten und Plotten sind sich in der Vorgehensweise sehr ähnlich.
Wo sich diese beiden Prozesse unterscheiden, ist im verwendeten Material und im Werkzeug, welches zum Schluss benötigt wird.

Zum **Laser-Cutten** benötigt man, einen Laser-Cutter.
Dies ist ein Gerät, welches ein Werkstück mittels eines Lasers zurecht schneiden und gravieren kann.
Diese Werkstücke können (auch je nach Cutter) aus Holz, Metall, Leder oder vielen anderen Materialien sein.

Zum **Plotten** wird ein sogenannter Plotter benötigt.
Dieser schneidet die Werkstücke mit einem kleinen Messer zurecht.
Dabei können einige Plotter auch mit anderen Aufsätzen bestückt werden und so beispielsweise Farbe auf ein Stück auftragen.

Am Anfang steht jedoch immer das Grafik-Design und dieses ist für beide Prozesse gleich.
Ein Plotter sowie auch ein Laser-Cutter benötigen eine Vektorgrafik, welche sie anschliessend verarbeiten können.
Eine solche Vektorgrafik (meist in Form des Containers .svg) findet sich im Internet zum Download, lässt sich aber mit einem Programm wie [Inkscape](https://inkscape.org) relativ schnell selber gestalten.

## Nützliches aus dem WWW

- [Grafikprogramm: Inkscape](https://inkscape.org)
- [SVG Puzzle-Generator](https://draradech.github.io/jigsaw/jigsaw.html)
