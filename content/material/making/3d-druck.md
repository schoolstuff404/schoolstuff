---
title: "3D-Druck"
date: 2023-10-04T18:56:32+02:00
draft: false
Author: "Aaron"
featured_image: '/images/default.jpg'
---

## Film

Modell wird gesliced (in Scheiben geschnitten) und vom 3D-Drucker Schicht für Schicht modelliert.
FDM-Verfahren mit PLA für den schulischen Einsatz, da andere mit Lasern und evtl. giftigen Chemikalien funktionieren.
SLS-Verfahren mit Pulver, so können auch Metalle gedruckt werden.
SLA-Verfahren.
Viel Open-Source, und Drucker sind auch als Bausatz erhätlich. So muss man sich effektiv mit der Materie auseinandersetzen.

### Wie funktioniert der 3D-Drucker
Über den Extruder wird das Filament vorwärtsbewegt, geschmolzen und als dünner Faden auf die Trägerplatte aufgetragen.

Slicing-Tool übernimmt das Slicing, hier werden die Druckparameter eingegeben und die Datei so für den Druck vorbereitet.
Benötigte Support-Strukturen werden von dem Slicing-Tool berechnet und dem Modell beigefügt.
Gewisse Steigungen kann der Drucker auch ohne Support-Strukturen bewältigen, dabei kann es jedoch auch passieren, dass der Druck durchhängt.

Die Support-Strukturen können nach dem Druck einfach abgebrochen oder anders entfernt werden, da sie sehr dünn gedruckt werden.

### Filamente

Es gibt Filamente mit Metallbestandteilen, so hat es eine höhere Dichte und kann geschliffen und poliert werden.
So entsteht ein Objekt, das optisch einem aus solidem Metall sehr ähnlich ist.

### Einsatzgebiete

3D-Druck ist sehr schnell, verglichen mit anderen Herstellungsverfahren, und eigenet sich so hervorragend für den Prototypenbau.
Auch für die Protesenherstellung ist der 3D-Druck sehr geeignet, da gerade Kinder schnell wachsen und immer wieder eine neue benötigen.

## Nützliches aus dem WWW

### CAD-Programme

Ein CAD-Programm (Computer Assisted Drawing) ist ein Tool, mit welchem 3D- und 2D Grafiken erstellt werden können.
Viele professionelle CAD-Programme sind kostenpflichtig und daher eher weniger für den Einsatz in der Schule geeignet.
Hier findest du eine Liste mit gratis verwendbaren Optionen:

- [ThinkerCAD](https://thinkercad.com) ist eine Cloud-Basierte 3D-CAD-Software.
  Die Software ist einfach zu bedienen und eignet sich somit gut für den Einsatz in der Schule.
- [FreeCAD](https://freecad.org) ist eine open-source 3D-CAD-Software für Windows, Mac und Linux.
  FreeCAD hat viele Funktionen und ist auch für den professionellen Einsatz geeignet.
  Jedoch ist die Software nicht sehr intuitiv und daher nur bedingt für den Einsatz in der Schule geeignet.
- [Blender](https://blender.org) ist eine sehr mächtige 3D-Grafik-Software für Windows, Mac und Linux.
  Es gibt nichts, das mit Blender nicht vollbracht werden kann, vom einfachen Modellieren bis zum Animieren ganzer Filme.
  Die Software hat eine steile Lernkurve und ist eher für fortgeschrittene Nutzer geeignet.

### Slicer

Um ein 3D-Modell für den Druck vorzubereiten, wird ein sogenanntes Slicing-Tool benötigt.
Diese schaffen die einzelnen Druck-Ebenen, füllen solide Elemente mit einer Gewebestruktur und berechnen Support-Strukturen.
Hier findest du eine Liste mit verschiedenen Slicing-Tools:

- [Ultimaker Cura](https://ultimaker.com/software/ultimaker-cura/) ist ein open-source Slicing-Tool, das für den Ultimaker entwickelt wurde.
  Dieses läuft auf Windows und Mac, und mit einigen Schwierigkeiten auch auf Linux.
  [Quellcode](https://github.com/Ultimaker/Cura)
- [PrusaSlicer](https://www.prusa3d.com/page/prusaslicer_424/) ist ein open-source Slicing-Tool für Windows, Mac und Linux.
  Der PrusaSlicer ist sehr vielseitig und lässt viele Anpassungen in den Druckeinstellungen zu.
  Dennoch ist das Tool einfach zu bedienen und somit ebenfalls für den Einsatz in der Schule geeignet. 
  [Quellcode](https://github.com/prusa3d/PrusaSlicer)

### Material

- [Thingiverse](https://thingiverse.com) ist eine Webseite, auf welcher viele Maker aus der ganzen Welt ihre 3D-Modelle miteinander teilen.
  Diese dürfen gratis verwendet werden und bieten somit eine solide Grundlage für das 3D-Drucken in der Schule.
  Falls ihr selber Modelle erstellt habt, die jemanden dienen könnten, so stellt diese doch bitte der Community ebenfalls zur Verfügung.

