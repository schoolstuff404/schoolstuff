---
title: "spielen"
menu: 
    main:
        parent: "material"
featured_image: '/images/default.jpg'
---

## Lernen durch Spielen?

Viele Kompetenzen des Lehrplans können auch durch das Spielen erlernt werden:

- Überfachiche Kompetenzen
    - Auftrittskompetenzen
    - Sozialfähigkeiten (Konfliktbewältigung)
- Wissen / Fachliche Kompetenzen
    - Leseverständnis
    - Mathematik
    - etc.

## DingsBox

Die DingsBox ist in einem ALGE-Projekt der PHSG entstanden und beinhaltet eine Sammlung an Ritualen und Spielen, die mit möglichst wenig bis keinem Material spielbar sind.

## Gute Spiele

- Würfelspiele
    - Quixx
