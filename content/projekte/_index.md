---
title: "projekte"
menu: main
weight: 8
featured_image: '/images/projects.jpg'
---

# [Die DingsBox](https://dingsbox.azureorange.xyz)

Die DingsBox ist eine einfache Sammlung an **Spielen**, welche ohne grosse Vorbereitung und mit wenig bis keinem Material gespielt werden können, sowie **Ritualen** für den Schulalltag.
Entstanden ist die DingsBox im Rahmen eines ALGE-Projekts an der PHSG.


# [Linux macht Schule](https://linux.azureorange.xyz)

Das Projekt **"Linux macht Schule"** ist im Rahmen meiner Bachelorarbeit entstanden und soll aufzeigen, warum die Arbeit mit Linux in der Primarschule sinnvoll ist.
Das Projekt besteht aus einigen Anleitungen und Unterrichtsmaterialien vor allem im Bereich **Medien und Informatik**.

