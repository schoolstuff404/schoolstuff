---
title: "planen"
menu: main
draft: true
weight: 2
featured_image: '/images/planning.jpg'
bib: 'bibliography'
---

## Jahresplanungen

- Im Team planen (Stufe, Team-Teaching)
- Ist eine Jahresplanung von der Schule verfügbar?
- Lehrplan - verbindliche Themen
- Idealerweise vor den Sommerferien machen

### Lehrplan

Hier finden Sie Informationen zum Lehrplan 21 der verschiedenen Kantone: [Lehrplan 21](https://www.lehrplan21.ch/)

## Grob- / Feinplanungen

- Vorwissen vor den Feinplanungen abfragen
- Handreichungen (Lehrpersonenkommentare) zu den Lehrmitteln konsultieren

### Allgemeines Planungsmodell PHSG

Rahmenmodell zur Planung kompetenzorientierten Unterrichts {{< cite "p" "wullschlegerbirri14" >}}

[![Rahmenmodell zur Planung kompetenzorientierten Unterrichts (Wullschleger; Birri 2014)](./rahmenmodell-zur-planung-kompetenzorientierten-unterrichts.png)](./rahmenmodell-zur-planung-kompetenzorientierten-unterrichts.pdf)


