---
title: "gestalten"
draft: true
menu: 
    main:
        parent: "planen"
featured_image: '/images/gestalten.jpg'
draft: true
---

# Lernformen

## Nachvollziehendes Lernen

Vorzeigen / Nachmachen
Videotutorial
Anleitung (schriftlich / bildlich)
Wekstattparcour

## Analysierendes Lernen

Auseinandernehmen / Anschauen, verstehen, zusammenbauen.

## Entdeckendes Lernen

Entdeckendes überlappt mit Analysierendem

Entdeckendes Lernen ist ein gestalterisches oder technisches Experiment mit einem Ausgang.
Bsp. Welches Material eignet sich am besten als Wurfgeschoss für mein Katapult? -> werfen und schauen, welches weiter kommt.

## Entwickelndes Lernen

Die entwickelnde Lernaufgabe eignet sich hervorragend als Transferaufgabe.
Die vorgängig erworbenen Kompetenzen werden angewendet und dabei wird ein individuelles Produkt entwickelt.


Bei einer entwickelnden Lernaufgabe steht ganz klar das Entwicklungspotential im Vordergrund.

Über die vorgegebenen Bedingungen kann gesteuert werden, dass von den Arbeiten der SuS die gewünschte Qualität erreicht wird.
Aus diesen Bedingungen wird anschliessend die Bewertung abgeleitet.

### Bedingungen

- Welche Resourcen stehen zur Verfügung?
- Wie viel Zeit steht zur Verfügung?
- Welche Ansprüche werden an das Produkt gestellt?


