---
title: "deutsch"
draft: true
menu: 
    main:
        parent: "planen"
featured_image: '/images/deutsch.jpg'
---

## Lehrmittel

- [Die Sprachstarken](https://www.klett.ch/lehrwerke/die-sprachstarken-1-6-neue-ausgabe)
- [Leseschlau](https://www.ursularickli.ch/Meine-Lehrmittel/Leseschlau/)

