---
title: "nmg"
draft: true
menu: 
    main:
        parent: "planen"
featured_image: '/images/nmg.jpg'
---

## Planungsmodell NMG PHSG

[![Planungsmodell NMG](./planungsmodell-nmg.png)](./planungsmodell-nmg.pdf)

## Lehrmittel

Auf diesen Seiten finden Sie Informationen zu den NMG-Lehrmitteln des Schulverlags:

- [Weitblick](https://weitblick-nmg.ch/)
- [NaTech](https://na-tech.ch/)
- [WAH](http://wahbuch.ch/)


