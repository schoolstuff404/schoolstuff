---
title: "mathematik"
draft: true
menu: 
    main:
        parent: "planen"
featured_image: '/images/mathematik.jpg'
---

## Rechenschwierigkeiten

Ein sehr gutes Buch, um Kinder mit Dyskalkulie verstehen zu lernen ist, [Rechenschwäche verstehen - Kinder gezielt fördern](https://www.persen.de/3503-rechenschwaeche-verstehen-kinder-gezielt-foerdern.html) von Michael Gaidoschik.

Nur mehr zu rechenen ist nicht immer die richtige Strategie.

Bei leistungsschwachen Rechner:innen sollte erforscht werden, wo genau die Schwierigkeiten liegen.
Dabei sollte den Überlegungen der Schüler:innen immer gut zugehört werden; diese sind auch bei fehlerhaften Resultaten oft gut.

(für Fussballbegeisterte hier ein [Interview mit Manuel Akanji](https://www.srf.ch/play/tv/sport-clip/video/manuel-akanji-ist-auch-ein-wandelnder-taschenrechner?urn=urn:srf:video:b5233bad-7f11-4129-94a9-276fa56e6acf))

### Material

Wenn Kinder Schwierigkeiten mit dem Rechnen haben, sollten sie immer die Möglichkeit haben, auf Unterstützungsmaterial zurückzugreifen.
Denn Mathematik zu lernen benötigt immer gute Visualisierungen.

Dabei ist es sehr hilfreich, wenn die Kinder zu Hause auf das gleiche Material zugreifen können wie in der Schule.

- [Mathe-Würfel](https://holzwuerfel.com/epages/f6a7c893-9c35-4f50-a2f8-a96f6a066b9a.sf/de_DE/?ObjectPath=/Shops/f6a7c893-9c35-4f50-a2f8-a96f6a066b9a/Categories/38)
- [PIKAS: Mathekartei](https://pikas.dzlm.de/unterricht/mathekartei)

