---
title: "SchoolStuff"
decription: "Hello World"
featured_image: '/images/default.jpg'
first_image: '/images/kindergarten.jpg'
first_title: '.kindergarten'
second_image: '/images/unterstufe.jpg'
second_title: '.unterstufe'
third_image: '/images/mittelstufe.jpg'
third_title: '.mittelstufe'
---

## Herzlich willkommen auf schoolstuff.ch.

Dieses Projekt richtet sich an Lehrpersonen, welche gerne ihre Schulmaterialien wie zum Beispiel Arbeitsblätter, Unterrichtsreihen bzw. -ideen etc. mit Kollegen aus der ganzen Schweiz teilen möchten.


