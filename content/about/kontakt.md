---
title: "kontakt"
menu: 
    main:
        parent: "about"
featured_image: '/images/default.jpg'
---

Möchtest du mich kontaktieren, so bin ich unter dieser [E-Mail](mailto:contact@azureorange.xyz) erreichbar.
