---
title: "mitmachen"
menu: 
    main:
        parent: "about"
featured_image: '/images/default.jpg'
---

Wenn du Anregungen zum Projekt hast oder Materialien zur Verfügung stellen möchtest, lade ich dich herzlich ein, einen **Merge Request** oder ein **Issue** in einem meiner [GitLab Repositories](https://gitlab.com/schoolstuff-ch/) zu eröffnen.
Deine Ideen und Beiträge sind wertvoll für die Weiterentwicklung des Projekts!

Für Fragen oder Anliegen stehe ich gerne zur Verfügung.
Du kannst mir jederzeit eine [E-Mail](mailto:contact@azureorange.xyz) senden.
Ich freue mich darauf, von dir zu hören!

Möchtest du dich aktiv am Projekt beteiligen?
Schau doch einmal in unserem [GitLab Repo](https://gitlab.com/schoolstuff-ch/) vorbei, um mehr darüber zu erfahren, wie du dich einbringen kannst und welche Möglichkeiten der Zusammenarbeit bestehen.

Deine Mitwirkung ist entscheidend, um das Projekt zu bereichern und einen Mehrwert für Lehrpersonen zu schaffen. Vielen Dank im Voraus für dein Engagement und deine Unterstützung!

