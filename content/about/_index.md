---
title: "about"
date: 2022-07-04T11:59:08+02:00
menu: main
weight: 10
featured_image: '/images/default.jpg'
---

Ich studiere zur Zeit an der Pädagogischen Hochschule in St. Gallen und habe mir überlegt, dass es toll wäre, wenn es einen Ort im Internet gäbe an welchem Lehrerinnen und Lehrer ihre Unterrichtsmaterialien austauschen können.

Zu diesem Zweck habe ich die Seite SchoolStuff.ch ins Leben gerufen und werde beginnen, alle Materialien, welche ich herstelle oder frei zur Verfügung stellen kann, hier abzulegen.

Mehr darüber im ersten [Blogbeitrag](/blog/posts/20220719_2031_crappy_draft/).

Wenn du Anregungen zum Projekt hast oder Materialien zu Verfügung stellen willst, erstelle einfach einen **Merge Request** oder ein **Issue** in meinem [Gitlab Repository](https://gitlab.com/schoolstuff404/schoolstuff) bzw. bei Fragen schreibe mir eine [E-Mail](mailto:azureorange404@protonmail.com).

Willst du dich am Projekt beteiligen lies doch einmal den [Wiki Eintrag](/wiki/contribute/) darüber.
