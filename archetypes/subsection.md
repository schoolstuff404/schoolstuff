---
title: "/{{ replace .Name "-" " " | title | lower }}"
menu: 
    main:
        parent: "/{{ index (split .Dir "/") 0 }}"
featured_image: '/images/default.jpg'
---

Nothing here yet ...
